import bpy

pboDataPath = "alphagarg_variety_pack_wads/materials/"
imageExtension = ".tga"

for m in bpy.context.object.material_slots:
    m.material.armaMatProps.texture = pboDataPath + m.material.name.rsplit('/',1)[0] + "/" + m.material.texture_slots[0].texture.image.name.replace(imageExtension,".paa")
    m.material.armaMatProps.rvMat = pboDataPath + m.material.name.rsplit('/',1)[0] + "/" + m.material.texture_slots[0].texture.image.name.replace(imageExtension,".paa")