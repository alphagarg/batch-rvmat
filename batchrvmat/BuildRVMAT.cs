﻿using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace batchrvmat
{
    public class BuildRVMAT
    {
        public static void Main()
        {
            var appPath = AppDomain.CurrentDomain.BaseDirectory;
            var dirInfo = new DirectoryInfo(appPath);
            var subDirs = dirInfo.EnumerateDirectories();

            List<DirectoryInfo> dirs = new List<DirectoryInfo>();
            dirs.Add(dirInfo); dirs.AddRange(subDirs);

            List<IEnumerable<DirectoryInfo>> subDirsEnumList = new List<IEnumerable<DirectoryInfo>>();
            subDirsEnumList.Add(subDirs);
            while (subDirsEnumList.Count > 0)
            {
                foreach (var di in subDirsEnumList[0])
                {
                    dirs.Add(di);

                    var newEnum = di.EnumerateDirectories();
                    int newEnumCount = 0;
                    foreach (var i in newEnum)
                        newEnumCount++;

                    if (newEnumCount > 0)
                        subDirsEnumList.Add(newEnum);

                    Console.WriteLine("{0} subfolders found in {1}", newEnumCount, di.Name);
                }
                subDirsEnumList.RemoveAt(0);
            }


            Console.WriteLine("\n\n\nPlease input the P: Drive path to the PBO, e.g. a3/structures_f/, CUP/Weapons/CUP_Weapons_VehicleWeapons/, alphagarg_variety_pack_wads/");
            Console.WriteLine("Make sure to use forward slashes, and remember the one at the end.");
            var pboPath = Console.ReadLine();
            Console.WriteLine("\n\nPlease input the image file extension I should look for, i.e. png, tga, paa, etc.");
            Console.Write("No preceding dots! Just the three or more letters after it: ");
            string imageExtension = $".{Console.ReadLine().ToLower()}";
            Console.WriteLine("\n\nNow, a question! Do I delete existing RVMATs I encounter, or do I leave them be?");
            Console.Write("Y/N: ");
            bool deleteExisting = Console.ReadKey().Key == ConsoleKey.Y;
            if (deleteExisting)
            {
                Console.WriteLine("\n\nAre you sure? This action cannot be undone! (I've always wanted to say that)");
                Console.Write("Y/N: ");
                deleteExisting &= Console.ReadKey().Key == ConsoleKey.Y;
            }
            Console.WriteLine("\n\n\nCommencing..");

            foreach (var di in dirs)
            {
                var files = di.EnumerateFiles();
                foreach (var fi in files)
                {
                    if (fi.Extension.ToLower() == imageExtension)
                    {
                        string rvmatPath = fi.FullName.Replace(imageExtension, ".rvmat");

                        if (File.Exists(rvmatPath) && deleteExisting)
                            File.Delete(rvmatPath);

                        if (deleteExisting || !File.Exists(rvmatPath))
                        {
                            string rvmat =
                            "ambient[] = {1,1,1,1};\n" +
                            "diffuse[] = {1,1,1,1};\n" +
                            "forcedDiffuse[] = {0,0,0,0};\n" +
                            "emmisive[] = {0,0,0,1};\n" +
                            "specular[] = {0,0,0,0};\n" +
                            "specularPower = 0;\n" +
                            "PixelShaderID = \"Normal\";\n" +
                            "VertexShaderID = \"Basic\";\n" +
                            "class StageTI\n" +
                            "{\n" +
                            $"\ttexture = \"{fi.FullName.Replace(appPath, pboPath).Replace(imageExtension, ".paa")}\";\n" +
                            "};\n";

                            var rvmatBytes = System.Text.Encoding.UTF8.GetBytes(rvmat);

                            var fs = File.Create(rvmatPath);
                            fs.Write(rvmatBytes, 0, rvmatBytes.Length);
                            fs.Close();
                        }

                        Console.WriteLine(fi.Name);
                    }
                }
            }


            Console.WriteLine("\n\n\nDone! Press the Any key to close application.");
            var endKey = Console.ReadKey().KeyChar;
            Console.WriteLine($"\nHey, that wasn't the Any key. That was {endKey}!");
            Console.ReadKey();
            Console.WriteLine("\n\nI'm just messing with ya. See ya later, alligator.");
        }
    }
}
